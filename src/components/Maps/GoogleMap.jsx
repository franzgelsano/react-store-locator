import React, { Component } from 'react';
import { GoogleApiWrapper, Map, Marker } from 'google-maps-react';

import './googleMap.scss';

class GoogleMap extends Component {
    render(){
        const { currentPosition } = this.props;
        return (
            <div className='map-container'>
                <Map 
                    google={this.props.google} 
                    zoom={14}
                    initialCenter={currentPosition}
                >
                    <Marker name={'Current Location'} />
                </Map>
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyDQk4YpaTcXTgw6e7u-6MSR2lfs0jJHT3A',
})(GoogleMap);