import React, { Component } from 'react';
import './searchResultItem.scss';
import { HomeFilled, TagsFilled } from '@ant-design/icons';

class SearchResultItem extends Component {
    render() {
        return (
            <div className='search-result-item-container'>
                <div className="result-item__name">
                    <span>Result Name</span>
                    <span className="result-item__distance">0 km</span>
                </div>
                <div className="result-item__description">
                    <HomeFilled className='tags-icon'/>
                    <span>Address</span>
                </div>
                <div className="result-item__tags">
                    <TagsFilled className='tags-icon'/>
                    <span className='tags-item'>Tags</span>
                </div>
            </div>
        );
    }
}

export default SearchResultItem;