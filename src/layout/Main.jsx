import React, { Component }from 'react';
import './main.scss';
import { Layout } from 'antd';
import Brand from '../features/brand/Brand';
import Search from '../features/search/Search';
import SearchResult from '../features/search-result/SearchResult';
import Map from '../components/maps/GoogleMap';

const { Sider, Content } = Layout;

class Main extends Component {
    state = {
        currentPosition: null,
    };

    componentDidMount(){
        window.navigator.geolocation.getCurrentPosition(({ coords }) => {
            const currentPosition = {
                lat: coords.latitude,
                lng: coords.longitude,
            };
            this.setState({ currentPosition });
        });
    }

    render(){
        return <div className='main-layout'>
            <Content className='content'>
                <Brand />
                <Search />
                <div className='search-results-container'>
                    <SearchResult className='search-result'/>
                    <Map 
                        className='search-map'
                        currentPosition={this.state.currentPosition}
                    />
                </div>
            </Content>
        </div>;
    }
}

export default Main;