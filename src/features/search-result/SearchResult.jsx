import React, { Component }from 'react';
import SearchResultItem from '../../components/search-result-item/SearchResultItem';

import './searchResult.scss';

class SearchResult extends Component {
    render(){
        return(
            <div className='search-result-container'>
                <SearchResultItem />
            </div>
        );
    }
}

export default SearchResult;