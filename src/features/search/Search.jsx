import React, { Component }from 'react';
import { Input, Select, Button } from 'antd';

import './search.scss';

const { Option } = Select;

class Search extends Component {
    render(){
        return(
            <div className='search-component'>
                <form className='search-component-form'>
                    <div className='search-option'>
                        <Input.Group compact>
                            <Button 
                                className='btn-search'
                                type='primary'
                                shape='round'
                                htmlType='submit'
                            >
                            Search
                            </Button>
                            <Select defaultValue='Distance'>
                                <Option value='Distance'>Distance (km)</Option>
                                <Option value='Current Location'>Current Location</Option>
                            </Select>
                            <Input 
                                name='distance'
                            />
                        </Input.Group>
                    </div>
                    <div className='search-boxes'>
                        <Input 
                            name='query'
                            className='search'
                            placeholder='Search an establishment (e.g. Grocery)'
                        />
                    </div>
                </form>
            </div>
        );
    }
}

export default Search;